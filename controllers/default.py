# -*- coding: utf-8 -*-


def index():
  response.title = response.company.name
  response.canonical = response.company.url

  return dict()


def company():
  return dict()


def projects():
  projects = db().select(db.project.ALL)
  return dict(projects=projects)


def contact():
  return dict()


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


