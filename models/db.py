# -*- coding: utf-8 -*-

if request.global_settings.web2py_version < "2.14.1":
    raise HTTP(500, "Requires web2py 2.13.3 or newer")

# -------------------------------------------------------------------------
# if SSL/HTTPS is properly configured and you want all HTTP requests to
# be redirected to HTTPS, uncomment the line below:
# -------------------------------------------------------------------------
# request.requires_https()

# -------------------------------------------------------------------------
# app configuration made easy. Look inside private/appconfig.ini
# -------------------------------------------------------------------------
from gluon.contrib.appconfig import AppConfig

# -------------------------------------------------------------------------
# once in production, remove reload=True to gain full speed
# -------------------------------------------------------------------------
myconf = AppConfig(reload=True)

if not request.env.web2py_runtime_gae:
    # ---------------------------------------------------------------------
    # if NOT running on Google App Engine use SQLite or other DB
    # ---------------------------------------------------------------------
    db = DAL(myconf.get('db.uri'),
             pool_size=myconf.get('db.pool_size'),
             migrate_enabled=myconf.get('db.migrate'),
             check_reserved=['all'])
else:
    # ---------------------------------------------------------------------
    # connect to Google BigTable (optional 'google:datastore://namespace')
    # ---------------------------------------------------------------------
    db = DAL('google:datastore+ndb')
    # ---------------------------------------------------------------------
    # store sessions and tickets there
    # ---------------------------------------------------------------------
    session.connect(request, response, db=db)
    # ---------------------------------------------------------------------
    # or store session in Memcache, Redis, etc.
    # from gluon.contrib.memdb import MEMDB
    # from google.appengine.api.memcache import Client
    # session.connect(request, response, db = MEMDB(Client()))
    # ---------------------------------------------------------------------

# -------------------------------------------------------------------------
# by default give a view/generic.extension to all actions from localhost
# none otherwise. a pattern can be 'controller/function.extension'
# -------------------------------------------------------------------------
response.generic_patterns = ['*'] if request.is_local else []
# -------------------------------------------------------------------------
# choose a style for forms
# -------------------------------------------------------------------------
response.formstyle = myconf.get('forms.formstyle')  # or 'bootstrap3_stacked' or 'bootstrap2' or other
response.form_label_separator = myconf.get('forms.separator') or ''

from gluon.tools import Service, PluginManager
from gluon.storage import Storage

service = Service()
plugins = PluginManager()

DEBUG = False

is_carousel = True
is_carousel_bg = True
is_carousel_indicators = True
is_carousel_controls = True
carousel_data_interval = 5000000

is_video = True
is_object_fit = True


company_name = "Provenic Ltd"
company_email = "info@provenic.com"
company_phone_number = "+358451306487"
company_url = "http://www.provenic.com"
company_address_street_name = "Mallastie 12 A 10"
company_address_postal_code = "90520"
company_address_city = "Oulu"
company_address_country = "Finland"
company_business_id = "2767937-5"
company_vat_number = "FI27679375"
company_logo = URL('static','images/provenic-logo.png', scheme=True, host=True)

company_facebook = "https://www.facebook.com/m5seppal"
company_twitter = None
company_linkedin = None
company_instagram = None
company_google = None
company_youtube = None
company_pinterest = None

contact_name = "Marko Seppälä"
contact_title = "CEO"
contact_email = "marko@provenic.com"
contact_phone_number = "+358451306487"


response.google_site_verification = "TuVz3Q7RSk6KPkzjYwxSY0YzcPpdMi-uG6KOaiWWQBc"

response.company = Storage(dict(
        name = company_name,
        email = company_email,
        phone_number = company_phone_number,
        url = company_url,
        street_name = company_address_street_name,
        postal_code = company_address_postal_code,
        city = company_address_city,
        country = company_address_country,
        business_id = company_business_id,
        vat_number = company_vat_number,
        logo = company_logo,
        linkedin = company_linkedin,
        facebook = company_facebook,
        instagram = company_instagram,
        twitter = company_twitter,
        google = company_google,
        youtube = company_youtube,
        pinterest = company_pinterest
        ))

response.contact = Storage(dict(
        name = contact_name,
        title = contact_title,
        email = contact_email,
        phone_number = contact_phone_number
        ))


#if request.uri_language:
#  T.force(request.uri_language)

T.force(request.lang)

'''  
T.set_current_languages('en', 'en-US')
if request.vars._language:
    session._language=request.vars._language
if session._language:
  T.force(session._language)
else:
  T.force(T.http_accept_language)
'''

# -------------------------------------------------------------------------
# Models
# -------------------------------------------------------------------------

db.define_table('project',
  Field('title', 'string', notnull=True),
  Field('description', 'text', notnull=True),
  Field('url', 'string', notnull=True),
  Field('image', 'upload', notnull=True)
  )
db.project.title.requires = IS_NOT_EMPTY()
db.project.description.requires = IS_NOT_EMPTY()
db.project.url.requires = IS_URL()
db.project.image.requires = IS_IMAGE()