require 'compass/import-once/activate'

http_path = "/"
css_dir = "css"
sass_dir = "scss"
images_dir = "images"
javascripts_dir = "js"

output_style = :expanded
# output_style = :compressed
# relative_assets = true
# line_comments = false
